<?php

namespace App;

use App\Modules\UserManagement\Traits\AccessGrantedRoleTrait;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use AccessGrantedRoleTrait;
    /**
     * The attributes that are fillable via mass assignment.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'special','category'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Roles can belong to many users.
     *
     * @return Model
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * Roles can have many permissions.
     *
     * @return Model
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class)->withTimestamps();
    }


    public function __toString()
    {
        return $this->name;
    }
}
