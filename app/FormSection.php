<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormSection extends Model
{
  protected $table = 'form_sections';

  protected $fillable = ['form_id','name','row','size','priority','created_by','notes','show'];

  public function form()
  {
      return $this->belongsTo(Form::class,'form_id');
  }

  public function fields()
  {
      return $this->hasMany(FormField::class,'form_section_id');
  }

  public function author()
  {
      return $this->belongsTo(User::class,'created_by');
  }

}
