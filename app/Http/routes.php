<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
$router->bind('users',function($users){
    return \App\User::whereId($users)->first();
});

$router->bind('roles',function($roles){
    return \App\Role::whereId($roles)->first();
});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
  Route::get('/', function () {
      //return Theme::view('welcome');
      return redirect()->route('get.login');
  });
});
