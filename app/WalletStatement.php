<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletStatement extends Model
{
    protected $table = 'wallet_statements';

    protected $fillable = [
        'wallet_id','money_in','amount','balance','txn_date','notes'
    ];

    public function wallet()
    {
        return $this->belongsTo(Wallet::class,'wallet_id');
    }
}
