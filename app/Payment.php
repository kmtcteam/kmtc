<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
        'payable_id','payable_type','payment_method','amount',
        'date_paid','notes','user_id'
    ];

    public function payable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
