<?php

namespace App\Widgets;

use App\Template;
use Arrilot\Widgets\AbstractWidget;

class NotificationTemplates extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $items = Template::all();


        return view("widgets.notification_templates", [
            'config' => $this->config,
        ],compact('items'));
    }
}