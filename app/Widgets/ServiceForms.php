<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class ServiceForms extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'service'=>null
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $service = $this->config['service'];

        $items = $service->forms;

        return view("widgets.service_forms", [
            'config' => $this->config,
        ],compact('items'));
    }
}