<?php

namespace App\Events;

use App\Events\Event;
use App\Wallet;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WalletTransaction extends Event
{
    use SerializesModels;

    public $wallet;
    public $in;
    public $notes;
    public $amount;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Wallet $wallet,$amount,$notes,$in = true)
    {
        $this->wallet = $wallet;
        $this->in = $in;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
