<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallets';

    protected $fillable = [
        'user_id','id_number','balance'
    ];


    public function statements()
    {
        return $this->hasMany(WalletStatement::class,'wallet_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function moneyIn($amount,$notes){

        $wallet = $this;
        \DB::transaction(function ($amount,$notes,$wallet) {
            \DB::table('wallet_statement')->insert([
                'wallet_id'=>$wallet->id,
                'money_id'=>1,
                'amount'=>$amount,
                'balance'=>$wallet->balance,
                'txn_date'=>date("Y-m-d H:i:s"),
                'notes'=>$notes
            ]);

            $balance = $wallet->balance + $amount;

            \DB::table('wallets')
                ->where('id',$wallet->id)
                ->update(['balance' => $balance ]);
        });

    }

    public function moneyOut($amount,$notes)
    {
        $return = false;
        $wallet = $this;

        \DB::transaction(function ($amount,$notes,$wallet) {
            \DB::table('wallet_statement')->insert([
                'wallet_id'=>$wallet->id,
                'money_id'=>0,
                'amount'=>$amount,
                'balance'=>$wallet->balance,
                'txn_date'=>date("Y-m-d H:i:s"),
                'notes'=>$notes
            ]);

            $balance = $wallet->balance - $amount;

            \DB::table('wallets')
                ->where('id',$wallet->id)
                ->update(['balance' => $balance ]);

            $return = true;
        });

        return $return;

    }


    public function getBalance()
    {
        $balance = 0;
        $wallet = $this;
        \DB::transaction(function ($amount,$notes,$wallet) {

            $res = \DB::table('wallets')
                ->where('id',$wallet->id)->get();

            if ($res){
                $balance = $res->balance;
            }
        });
    }

}
