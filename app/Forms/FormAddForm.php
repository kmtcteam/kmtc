<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\User;
class FormAddForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name','text',[
                'rules'=>'required|unique:field_types,name'
             ])
             ->add('category_id','select',[
                 'choices'=> getAllData(\App\Category::class,'id','name',false),
                 'rules'=>'required|exists:categories,id',
                 'label'=>'Category'
             ])
            ->add('service_id','select',[
                'choices'=> getAllData(\App\Service::class,'id','name',false),
                'rules'=>'required|exists:services,id',
                'label'=>'Service',
                'default'=>$this->getData('service_id')
            ])
             ->add('fee','number',[
                 'rules'=>'required',
                 'default'=>0,
                 'value'=>0
              ])
            ->add('notes','textarea',[
                'rules'=>'sometimes'
            ])
            ->add('created_by','hidden',[
                'value'=> user()->id,
                'rules'=>'required',
                'label'=>'Select Author'
            ])
            ->add('save', 'submit',  [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'Save Record'
            ]);
    }
}
