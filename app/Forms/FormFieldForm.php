<?php namespace App\Forms;

use App\FieldType;
use Kris\LaravelFormBuilder\Form;
use App\User;
class FormFieldForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('form_section_id','select',[
                'choices'=> $this->getData('form_sections'),
                'rules'=>'required',
                'label'=>'Select Section'
            ])
            ->add('field_type_id','select',[
                'choices'=> getAllData(FieldType::class,'id','name',false),
                'rules'=>'required',
                'label'=>'Field Type'
            ])
            ->add('name','text',[
                'rules'=>'required'
            ])
            ->add('rules','text',[
                'rules'=>'sometimes'
            ])
            ->add('size','number',[
                'rules'=>'sometimes|min:1|max:12',
                'label'=>'Size (Bootstrap column size 1-12)'
            ])
            ->add('priority','number',[
                'rules'=>'required|min:1',
                'label'=>'Priority: order with regard to other fields'
            ])
            ->add('query','textarea',[
                'rules'=>'sometimes',
                'label'=>'Data source: select key,value from table or a,b,c,d or a=>b,c=>d'
            ])

            ->add('db','checkbox',[
                'label'=>'Fetch from database?'
            ])
            ->add('created_by','hidden',[
                'default'=> user()->id,
                'value'=>user()->id,
                'rules'=>'required',
                'label'=>'Select Author'
            ])
            ->add('save', 'submit',  [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'Add Field'
            ]);
    }
}
