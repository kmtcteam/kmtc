<?php namespace App\Forms;

use App\User;
use Kris\LaravelFormBuilder\Form;

class FieldTypeForm extends Form
{
    public function buildForm()
    {
        $this->add('name','text',[
            'rules'=>'required|unique:field_types,name'
        ])
            ->add('created_by','select',[
                'choices'=> User::all()->lists('name','id')->toArray(),
                'rules'=>'required',
                'label'=>'Select Author'
            ])
            ->add('save', 'submit',  [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'Save Record'
            ]);
    }
}
