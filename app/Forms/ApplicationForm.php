<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Form as MyForm;
use App\User;
use Illuminate\Support\Facades\DB;
class CustomForm extends Form
{
    public function buildForm()
    {
        $myForm = $this->getData('form');

        foreach ($myForm->fields as $field)
        {

            if (in_array(str_slug($field->type->name),['select','choices'])){
                //dd($this->getQueryArray($field));
                $this->add('form_field_id['.$field->id.']',str_slug($field->type->name),[
                    'choices'=>$this->getQueryArray($field),
                    'rules'=> $field->rules,
                    'label'=>$field->name
                ]);
            }else{
                $this->add('form_field_id['.$field->id.']',str_slug($field->type->name),[
                    'rules'=> $field->rules,
                    'label'=>$field->name
                ]);
            }
        }

        $this
            ->add('resource_id','hidden',[
                'rules'=>'required',
                'value'=>$this->getData('resource_id'),
            ])
            ->add('form_id','hidden',[
                'rules'=>'required|exists:forms,id',
                'value'=>$myForm->id,
            ])
            ->add('created_by','select',[
                'choices'=> getAllData(User::class,'id','name'),
                'rules'=>'required',
                'label'=>'Select Author'
            ])
            ->add('save', 'submit',  [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'Save Record'
            ]);

    }

    public function getQueryArray($field)
    {
        $data = array();

        if ($field->db){
            $data = $this->ResultArray($field->query);
        }else{
            //split string into array example a,b,c,d;
            $values = explode(',',$field->query);

            //check if it is an associated array a:b,c:d,
            if (str_contains(':',$values[0])){

                //if it is an associated array assing the key value pair respectively a => b, c => d etc
                foreach($values as $val){
                    $data[$va[0]=$val[1]];
                }
            }else{
                $data = $values;
            }
        }

        return $data;
    }

    public function ResultArray($query)
    {
        try {
            $values = [];
            $result = DB::select($query);
            //dd(array_flatten($result));
            foreach ($result as $array){
                //convert object to array
                $array = (array)$array;

                //get the array keys
                $keys = (array_keys($array));

                //get array elements by their keys
                $values[$array[$keys[0]]] = $array[$keys[1]];
            }

            return $values;
        }catch (\Exception $e){

            return array($e->getMessage());
        }

    }
}
