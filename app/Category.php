<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $table = 'categories';

  protected $fillable = ['name'];

  public function __toString()
  {
    return $this->name;
  }

  public function courses()
  {
    return $this->hasMany(Form::class,'category_id');
  }
}
