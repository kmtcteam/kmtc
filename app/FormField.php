<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{
  protected $table = 'form_fields';

 protected $fillable = [
     'name','form_id','form_section_id','field_type_id','priority',
     'required','attributes','query','created_by','rules','db','size'
 ];

 public function form()
 {
     return $this->belongsTo(Form::class,'form_id');
 }

 public function section()
 {
     return $this->belongsTo(FormSection::class,'form_section_id');
 }

 public function type()
 {
     return $this->belongsTo(FieldType::class,'field_type_id');
 }

 public function author()
 {
     return $this->belongsTo(User::class,'created_by');
 }
 
}
