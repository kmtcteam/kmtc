<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 9/7/2015
 * Time: 1:31 PM
 */

namespace App\Libs;
use App\Form as MyForm;
use App\User;
use Illuminate\Support\Facades\DB;
use App\FormSection;
//use Illuminate\Html\FormFacade as Form;
use App\FormField;
class MyFormBuilder
{

    protected $form;
    protected $url;
    protected $files;
    protected $method;
    protected $fields = array();
    protected $sections = array();
    protected $html;
    protected $buttons =[];
    protected $extraFields = [];
    protected $formAttributes;
    protected $stepy = 0;
    protected $view = false; //for viewing filled application turns off input fields
    protected $data = [];

    public function __construct(MyForm $form,$url="#", $method = 'POST',$files = false,$attributes = [])
    {
        $this->form = $form;
        $this->url = $url;
        $this->method = $method;
        $this->files = $files;
        $this->sections = $form->sections;
        $this->fields = $form->fields;
        $this->formAttributes = $attributes;

        return $this;
    }

    public function getQueryArray($field)
    {
        $data = array();

        if ($field->db){
            $data = $this->ResultArray($field->query);
        }else{
            //split string into array example a,b,c,d;
            $values = explode(',',$field->query);

            //check if it is an associated array a:b,c:d,
            if (str_contains(':',$values[0])){

                //if it is an associated array assing the key value pair respectively a => b, c => d etc
                foreach($values as $val){
                    $data[$va[0]=$val[1]];
                }
            }else{
                $data = $values;
            }
        }

        return $data;
    }

    public function strToOptions($string)
    {
        $data = array();
        //split string into array example a,b,c,d;
        $values = explode(',',$string);

        //check if it is an associated array a:b,c:d,
        if (str_contains(':',$values[0])){

            //if it is an associated array assing the key value pair respectively a => b, c => d etc
            foreach($values as $val){
                $data[$va[0]=$val[1]];
            }
        }else{
            $data = $values;
        }

        return $data;
    }

    public function ResultArray($query)
    {
        try {
            $values = [];
            //dd($query);
            $result = DB::select($query);
            //dd(array_flatten($result));
            foreach ($result as $array){
                //convert object to array
                $array = (array)$array;

                //get the array keys
                $keys = (array_keys($array));

                //get array elements by their keys
                $values[$array[$keys[0]]] = $array[$keys[1]];
            }

            return $values;
        }catch (\Exception $e){

            return array($e->getMessage());
        }

    }

    public function buildSection(FormSection $formSection)
    {
        if ($this->stepy){
            $html =  '<fieldset class="step" title="'.$formSection->name.'" id="default-step-'.($formSection->id-1).'">
                    <legend>'.$formSection->name.'</legend>
                    <p><code>'.$formSection->notes.'</code></p>
                <div class="row">
                    '.$this->renderSection($formSection).'
                </div>
                </fieldset>
            ';
        }else{
            $html =  '<fieldset class="" title="'.$formSection->name.'" >
                    <legend>'.$formSection->name.'</legend>
                    <p><code>'.$formSection->notes.'</code></p>
                <div class="row">
                    '.$this->renderSection($formSection).'
                </div>
                </fieldset>
            ';
        }

        return $html;

    }

    public function renderSection(FormSection $formSection)
    {
        $fields = '';
        //dd($this->fields);
        foreach($formSection->fields as $field)
        {
            $fields .= $this->FormFieldTemplate($field);
        }
        //dd($fields);

        return $fields;
    }
    public function FormFieldTemplate(FormField $formFields){
        $label  = \Form::label('form_field_id_'.$formFields->id, $formFields->name);
        if ($this->view){
            $label = '<label> '.$formFields->name.' </label>';
        }

        $inputField = $this->buildField($formFields);
        if ($this->view){
            $inputField = $this->getData($formFields->id);
        }

        $field =  '
            <div class="form-group '.$this->getSize($formFields->size).'">
                '.$label.'
                    '.$inputField.'
            </div>';

        //dd($formFields->id.'-'.$this->getData($formFields->id));
        //dd($field);

        return $field;


    }

    public function buildField(FormField $formFields)
    {
        $dataType = $formFields->type->name;

        switch ($dataType){
            case in_array($dataType,['text']):
                $field = \Form::text("form_field_id[".$formFields->id."]",old($formFields->id),['class'=>'form-control']);
                break;
            case in_array($dataType,['select','option','options','choices']):
                $values = $this->getQueryArray($formFields);
                $field = \Form::select("form_field_id[".$formFields->id."]",$values,old($formFields->id),['class'=>'form-control']);
                break;
            default:
                $field = \Form::input($dataType,"form_field_id[".$formFields->id."]",old($formFields->id),['class'=>in_array($dataType,['checkbox','radio'])?'':'form-control']);
                break;
        }

        return $field;

    }

    public function buildCustomField($field)
    {
        $dataType = $field['type'];

        switch ($dataType){
            case in_array($dataType,['text']):
                $fld = \Form::text($field['name'],$field['value'] ,['class'=>'form-control']);
                break;
            case in_array($dataType,['select','option','options','choices']):
                $values = $this->strToOptions($field['options']);
                $fld = \Form::select($field['name'],$values,$field['value'],['class'=>'form-control']);
                break;
            default:
                $fld = \Form::input($dataType,$field['name'],$field['value'],['class'=>in_array($dataType,['checkbox','radio'])?'':'form-control']);
                break;
        }
        if ($dataType == 'hidden')
            return $fld;


        $tpl = "<div class='form-group".$this->getSize($field['size'])."'>
                ".\Form::label('form_field_id_'.$field['label'], $field['name'])."
                    ".$fld."
            </div>";

        return $tpl;

    }

    public function getSize($size)
    {
        return "col-md-$size col-sm-12 col-xs-12 col-lg-$size";
    }

    public function addField($name,$type='text',$options=[])
    {
        $field = [];
        $field['name'] = slugify($name);
        $field['type']=$type;
        if (!array_key_exists('label',$options)){
            $field['label']= $name;
        }else{
            $field['label']=$options['label'];
        }

        if (!array_key_exists('value',$options)){
            $field['value']='';
        }else{
            $field['value']=$options['value'];
        }

        if (!array_key_exists('options',$options)){
            $field['options']=[];
        }else{
            $field['options']=$this->strToOptions($options['options']);
        }

        if (!array_key_exists('size',$options)){
            $field['size']=12;
        }else{
            $field['size']=$options['size'];
        }

        $this->extraFields[$name] = $field;

        return $this;
    }

    public function addButton($name,$type = 'button',$attributes=[])
    {
        $attributes['class']= ($type =='submit' && $this->stepy)?'finish btn btn-primary':'btn btn-primary';
        $this->buttons[$name] = [
            'name'=>$name,
            'type'=>$type,
            'attributes'=>$attributes
        ];

        return $this;
    }

    public function renderButton(array $button)
    {
        $btn = '';
        if ($button['type'] == 'submit'){
            $btn = \Form::submit($button['name'],$button['attributes']);
        }else{
            $btn = \Form::button($button['name'],$button['attributes']);
        }

        return $btn;
    }

    public function renderSteps()
    {
        $i = 0;
        $steps = '<div class="stepy-tab">
         <ul id="default-titles" class="stepy-titles clearfix">';
        foreach ($this->sections as $section){
            $step = '';
            if ($i ==0){
                $step = 'current-step';
            }
            $steps .='<li id="default-title-'.($section->id-1).'" class="'.$step.'">
                          <div> Step '.++$i.'</div>
                      </li>
            ';
            $i++;
        }
        $steps .=' </ul></div>';
        return $steps;
    }
    public function render()
    {
       // $this->view = false;

        $form = \Form::open(['url'=>$this->url,'method'=>$this->method,'class'=>'', 'files'=>$this->files]);
        foreach ($this->sections as $section)
        {
            $form .= $this->buildSection($section);
        }

        foreach ($this->extraFields as $field){
            $form .= $this->buildCustomField($field);
        }

        $form .='<br><div class="form-group">';
        foreach ($this->buttons as $button){
            $form .= $this->renderButton($button);
        }
        $form .='</div>';
        $form .= \Form::close();

        $this->html = $form;

        return $this->html;
    }

    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function getData($key,$default = '')
    {
        if (!array_key_exists('form_field_id',$this->data))
            return;

        if (array_key_exists($key,$this->data['form_field_id'])){
            return $this->data['form_field_id'][$key];
        }

        return $default;
    }

    public function renderApplication()
    {
        $this->view = true;

        return $this->render();
    }

    public function renderStepy()
    {
        $this->stepy =1;
        $form = $this->renderSteps();

        $form .= \Form::open(['url'=>$this->url,'method'=>$this->method,'class'=>'form-stepy']);
        foreach ($this->sections as $section)
        {
            $form .= $this->buildSection($section);
        }

        foreach ($this->extraFields as $field){
            $form .= $this->buildCustomField($field);
        }

        //$form .='<div class="form-group">';
        foreach ($this->buttons as $button){
            $form .= $this->renderButton($button);
        }
        //$form .='</div>';
        $form .= \Form::close();

        $this->html = $form;

        return $this->html;
    }
}
