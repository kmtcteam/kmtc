<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Stevebauman\EloquentTable\TableTrait;

class FieldType extends Model
{
  use TableTrait;
  protected $table = 'field_types';

  protected $fillable = ['name','created_by'];

  public function author()
  {
      return $this->belongsTo(User::class,'created_by');
  }

}
