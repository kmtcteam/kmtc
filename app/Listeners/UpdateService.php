<?php

namespace App\Listeners;

use App\Events\BillPaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateService
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillPaid  $event
     * @return void
     */
    public function handle(BillPaid $event)
    {
        $bill = $event->bill;
        $service = $bill->getService();

        $service->status = 1;
        $service->paid;
        $service->save();
    }
}
