<?php

namespace App\Listeners;

use App\Events\ServiceRan;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogService
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceRan  $event
     * @return void
     */
    public function handle(ServiceRan $event)
    {
        //
    }
}
