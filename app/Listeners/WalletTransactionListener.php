<?php

namespace App\Listeners;

use App\Events\WalletTransaction;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WalletTransactionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WalletTransaction  $event
     * @return void
     */
    public function handle(WalletTransaction $event)
    {
        $wallet = $event->wallet;

        if ($event->in){
            $wallet->moneyIn($event->amount,$event->notes);
        }

    }
}
