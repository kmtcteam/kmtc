@extends(Theme::getActive().'::layouts.front')

@section('container')
    <table class="table table-striped table-bordered table-hover dt-responsive dataTables" width="100%">
        <thead>
        <tr>
            <th>Bill #</th>
            <th>Service</th>
            <th>Date Created</th>
            <th>Cost</th>
            <th>Total</th>
            <th>Status</th>
            <th>Action(s)</th>
        </tr>
        </thead>
        <tbody>

        @foreach($items as $item)
            <tr>
                <td>{{$item->bill_no}}</td>
                <td>{{$item->billable}}</td>
                <td>{{date('M d, Y',strtotime($item->created_at))}}</td>
                <td>KES {{number_format($item->cost,2)}}</td>
                <td>KES {{number_format($item->total,2)}}</td>
                <td>{!! status($item->paid,['<i class="fa fa-check"> Paid</i>','success'],['<i class="fa fa-times"> Unpaid</i>','danger']) !!}</td>
                <td>
                    <a href="{{route('my.bill',$item->id)}}" class=""> <i class="fa fa-eye"></i> Show</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop


@push('modals')

@endpush
