@extends(Theme::getActive().'::layouts.front')

@section('container')
    <h1>Welcome {{user()}}</h1>
    <div class="row">
        <!--blog start-->
        <div class="col-md-12 ">
          <table class="table table-striped table-hover dataTables">
                  <thead>
                  <tr>
                      <th>App Name</th>
                      <th>App No.</th>
                      <th>Service Name</th>
                      <th>Submission Date</th>
                      <th>Publication Status</th>
                      <th>Approval Status</th>
                      <th>Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($items as $item)
                      <tr>
                          <td>{{$item->form}}</td>
                          <td># {{$item->id}}</td>
                          <td>{{$item->applicable}}</td>
                          <td>{{$item->submission_date}}</td>
                          <td>{!!  status($item->published,'Published','Draft') !!}</td>
                          <td>{!! dynaStatus($item->status_id)!!}</td>
                          <td><a class="btn btn-success btn-sm" href="{{route('fill.form',$item->token)}}"><i class="fa fa-check-square-o"></i> Complete Application</a> </td>
                      </tr>
                  @endforeach
                  </tbody>
              </table>
        </div>


        <!--blog end-->
    </div>

@stop


@push('modals')

@endpush
