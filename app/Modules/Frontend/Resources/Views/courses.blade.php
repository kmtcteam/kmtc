@extends(Theme::getActive().'::layouts.front')

@section('container')

    {!!
      $items->columns(array(
          'id' => '#',
          'name' => 'Name',
          'fee'=>'Application Fee',
          'actions'=>'Action(s)'
      ))
      ->modify('fee',function($item){
        return 'KES '.number_format($item->fee,2);
      })
       ->modify('actions', function($item) {

            return '
              <a class="btn btn-info btn-sm" href="'.route('get.application',$item->id).'"> <i class="fa fa-pencil"></i> Apply </a>

            ';
        })
        ->attributes(array(
            'id' => 'table-1',
            'class' => 'table table-striped dataTables',
        ))
      ->showPages()
      ->render()
    !!}
@stop


@push('modals')

@endpush
