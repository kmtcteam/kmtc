<?php
namespace App\Modules\Frontend\Http\Controllers;

use App\Events\PaymentReceived;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
	public function dashboard()
	{

		//dd(user());
		//dd('here');//
		return \Theme::view('frontend::dashboard')
				->with('menu','profile');
	}

	public function courses($category)
	{
		//dd($category);
		$items = $category->courses()->published()->get();

        return \Theme::view('frontend::courses',compact('items'))
            ->with('menu','profile');
	}

	public function apply($form)
	{
		return $form;
	}

	public function myApplications()
	{
		$user = user();
		$items = $user->applications()->published()->get();

        return \Theme::view('frontend::applications',compact('items'))
            ->with('menu','profile');
	}


	public function myDraftApplications()
    {
        $items =\App\Application::myApplications(user()->id)->published(0)->get();

        return \Theme::view('frontend::drafts',compact('items'))
            ->with('menu','profile');
    }

	public function myBills()
	 {
         $items = \App\Bill::myBills()->latest()->get();

         return \Theme::view('frontend::bills',compact('items'))
                 ->with('menu','profile');
	 }


	 public function showBill($item)
	 {
         $bill = $item;
			 return \Theme::view('frontend::bill',compact('item','bill'))
					 ->with('menu','profile');
	 }

	public function verifyBillPayment($item)
	{

		if ($item->paid)
		{
			return redirect()->back();
		}

		$user = user();
		$wallet = $user->wallet;

		if (!$wallet){
			setFlash("error_msg","You do not have any money in your account, please top up to pay");

			return redirect()->back();
		}





		if ($wallet->getBalance() >= $item->total ){
			$payment = createPayment($item,$item->total,"Payment for $item");

			setFlash('success_msg',"Bill has been paid successfully, your application $item->billable has been submitted");
		}

		$received = $item->getTotalPaid();
		$balance = $item->getBalance();

		setFlash("error_msg","KES ".number_format($received,2)." has been paid. Please pay the remain balance of KES ".
					number_format($balance,2)." to submit your application $item->billable");

		return redirect()->back();

	}



}
