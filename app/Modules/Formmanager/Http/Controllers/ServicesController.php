<?php
namespace App\Modules\FormManager\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
	public function index()
    {
        $items = Service::all();

        return \Theme::view('formmanager::services.index',compact('items'));
    }


	public function create()
    {
        return \Theme::view('formmanager::services.new');
    }


	public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'code'=>'required|unique:services,code',
            'user_id'=>'required|exists:users,id',
            'description'=>'required'
        ]);

        $service = Service::create($request->all());

        setFlash("success_msg","Service $service successfully created");

        return redirect()->back();
    }

	public function show($item)
    {
        $service = $item;

        //dd($service);
        return \Theme::view('formmanager::services.show',compact('service'));
    }

	public function edit($item)
    {
        $service = $item;
        return \Theme::view('formmanager::services.edit',compact('service'));
    }

	public function update(Request $request,$item)
    {
        $this->validate($request,[
            'name'=>'required',
            'code'=>'required|unique:services,code',
            'user_id'=>'required|unique:users,id',
            'description'=>'required'
        ]);

        $item->update($request->all());

        setFlash("success_msg","Service $item successfully created");

        return redirect()->back();
    }

	public function destroy($item)
    {

    }
}
