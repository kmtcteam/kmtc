<?php
namespace App\Modules\FormManager\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FieldType;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\FieldTypeForm;
use Kris\LaravelFormBuilder\FormBuilderTrait;
class FieldTypesController extends Controller
{
	use FormBuilderTrait;
    protected $type;

    public function __construct(FieldType $fieldType)
    {
        $this->type = $fieldType;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(FormBuilder $formBuilder)
    {
        $items = $this->type->all();

        $form = $formBuilder->create(FieldTypeForm::class,[
            'method' => 'POST',
            'url' => route('fields.store')
        ]);

        return \Theme::view('formmanager::fieldtypes.index',compact('form','items'))
            ->with('page_title','Field Types');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(FieldTypeForm::class,[
            'method' => 'POST',
            'url' => route('fields.store')
        ]);

        return  \Theme::view('formmanager::fieldtypes.new',compact('form'))
            ->with('page_title','Field Types');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //initiate form builder with form class
        $form = $this->form(FieldTypeForm::class);

        $user = getUser();
        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = FieldType::create($request->all());
        //create alert
        //notify('Field type added',"$user->name added field type $data->name",'success','fa-cogs');

        //add session message and redirect
        setFlash('success_msg','Record updated successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
