<?php
namespace App\Modules\FormManager\Http\Controllers;

use App\Application;
use App\Form;
use App\FormField;
use App\Forms\CustomForm;
use App\FormSection;
use App\Property;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Forms\FormSectionForm,App\Forms\FormFieldForm;
use App\FormData;

class FormManagementController extends Controller
{
    use FormBuilderTrait;

    public function postAddFormSection($formId, Request $request)
    {
        $item = Form::findOrFail($formId);
        //initiate form builder with form class
        $form = $this->form(FormSectionForm::class);

        $user = user();
        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $input = $request->all();
        $input['form_id'] = $item->id;

        $data = FormSection::create($input);

        //create alert
        //notify('Form edit',"$user->name added section to form $item->name",'info','fa-cogs');

        //add session message and redirect
        setFlash('success_msg','Record updated successfully');
        return redirect()->back();
    }

    public function postAddFormField($formId, Request $request)
    {
        //dd(user());

        //dd($request->all());
        $item = Form::findOrFail($formId);
        //initiate form builder with form class
        $form = $this->form(FormFieldForm::class);

        $user = user();
        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $input = $request->all();
        $input['form_id'] = $item->id;

        $data = FormField::create($input);

        //create alert
        //notify('Form edit',"$user->name added field to form $item->name",'info','fa-cogs');

        //add session message and redirect
        setFlash('success_msg','Record updated successfully');
        return redirect()->back();
    }

    public function getEditFormField($id)
    {
        $field = FormField::findOrFail($id);
    }

    public function publishForm($form)
    {
        if (!$form->service){
            setFlash("error_msg","Form $form must be attached to a Service");
            return redirect()->back();
        }

        $form->status = 1;
        $form->save();


        setFlash("success_msg","Form $form has been published successfully!");
        return redirect()->route('forms.index');

    }

    public function getCreateApplication(Request $reques,$form)
    {
        //dd('ehre');

        /*
    $this->validate($request,[
        'type'=>'required',
        'resource'=>'required|exists:'.$request->get('type').',id'
    ],[
        'type.in'=>'Invalid application type',
        'resource.exists'=>'Resource not found'
    ]);

            */
        $user = user();
        $service = $form->service;

        //check for preset rent form

        $class = (get_class($user));
        if ($service){
            //check if application exists, if so, redirect user to application
            if ($service->limits){
                $apps = $service->applications()->whereNotIn('status_id',[4,3])->whereUserId($user->id)->first();
                if ($apps){
                    setFlash('info_msg','You have already made an application for '.$service);
                    return redirect()->back();
                }
            }

            $exists  = $service->applications()
                ->whereUserId(user()->id)
                ->whereFormId($form->id)
                ->whereBilled(0)
                ->first();


            if ($exists){

                //only allow if the applicatio has been rejection or for review
                if ($exists->published && !in_array($exists->status_id, [4,3])){
                    setFlash('info_msg','Your application already exists');
                    return redirect()->route('my.applications');
                }

                $data = $exists->getData();

                //dd($data);
                setFlash('info_msg','Your application already exists, complete the form');
                return redirect()->route('fill.form',$exists->token)
                    ->withInput($data);
            }
        }



        //if application does not exists create a new instance and redirect user to form
        $application = [
            'application_no'=>gen_app_no(),
            'user_id'=>$user->id,
            'form_id'=>$form->id,
            'applicable_id'=>$form->service ? $form->service->id: $form->id,
            'applicable_type'=>$form->service ? get_class($form->service): get_class($form),
            'created_by'=>$user->id,
            'token'=>md5(uniqid(time())),
            'status_id'=>1
        ];

        $app = Application::create($application);

        return redirect()->route('fill.form',$app->token);
    }

    public function getFillForm(Request $request,$app)
    {

        $form =$app->form;
        $item = $app->applicable;
        //dd($item);
        $fm= myForm($form,route('save.form',$app->token),'POST',true);
        $fm->addButton('Save Form','submit');
        $fm->addField('application_id','hidden',[
            'value'=>$app->id
        ]);
        $fm->addField('form_id','hidden',[
            'value'=>$form->id
        ]);
        $fm->addField('published','checkbox',[
            'value'=>1
        ]);

        $data = $app->getData();
        $request->session()->flashInput($data);
        //dd($form->render());

        return \Theme::view('frontend::application',compact('item','fm'))
            ->with('menu','properties');
    }

    public function postSaveFormData(Request $request,$app)
    {
        //dd($request->all());
        //initiate form builder with form class
        $item = $app->form;
        $dbFields = $item->fields->lists('name','id')->toArray();
        $presets = $item->fields->lists('rules','id')->toArray();
        $rules = [
            'form_id'=>'required|exists:forms,id',
            'application_id'=>'required|exists:applications,id'
        ];

        //load rules from the database
        foreach ($presets as $key => $val){
            $rules['form_field_id.'.$key] = $val;
        }

        $messages = [];
        foreach($dbFields as $key => $val)
        {
            $messages['form_field_id.'.$key.'.required'] = 'The field '.$val.' is required';
        }

        $this->validate($request,$rules,$messages);

        $user = user();

        //create db insert array
        $data = [];
        //if ($request->has)
        $inputs = $request->all();
        //dd($request->all());
        $files = $request->files->get('form_field_id');
        $fields = $inputs['form_field_id'];

        foreach($fields as $key => $val){
            $file = array_get($files,$key);
            if ($file){
                //dd('hasFile');
                $path = upload($file,$prefix = $app->id);

                //dd($path);
                $val = $path;
            }


            $data[] = [
                'form_field_id'=>$key,
                'value'=>$val,
                'application_id'=>$app->id,
                'created_by'=>$user->id
            ];
        }


        $app->saveForm($data);

        //if user has select to publish form, the save application as published
        if ($request->get('published')){
            if ($app->form->fee){
                //application should be billed before submission
                $bill = $app->generateBill();
                if($bill){

                    setFlash('success_msg','Please pay this bill to complete your submission!');
                    return redirect()->route('my.bill',$bill->id);
                }else{
                    setFlash('success_msg','Sorry your request could not be completed');
                    return redirect()->back();
                }
            }
            $app->update(['published'=>1,'submission_date'=>date("Y-m-d H:i:s")]);

            //('Rent Application',"$user->name submitted form $item->name",'info','fa-cogs');

            //add session message and redirect
            setFlash('success_msg','Form submitted successfully, remember add some functions to this process');
        }else{
            //create alert
            //notify('Rent Application',"$user->name updated form $item->name",'info','fa-cogs');

            //add session message and redirect
            setFlash('success_msg','Form saved successfully, remember add some functions to this process');
        }


        return redirect()->route('my.applications');
    }

    public function postDeleteApplication(Request $request,$app)
    {
        if (!$app->id){
            setFlash('error_msg','Invalid operation');
            return redirect()->back();
        }

        $app->deleteData();

        $app->delete();

        setFlash('success_msg','Application removed successfully');
        return redirect()->back();
    }

    public function publishApplication(Request $request,$app)
    {
        $this->validate($request,[
            'bill_id'=>'required|exists:bills,id'
        ]);
			$item = $app->form;
			$user = user();
			$bill = \App\Bill::findOrFail($request->get('bill_id'));

			//create db insert array
			$data = [];

			//if user has select to publish form, the save application as published
			if ($bill->paid){

                $app->submit();

                //('Rent Application',"$user->name submitted form $item->name",'info','fa-cogs');

                //add session message and redirect
                setFlash('success_msg','Form submitted successfully');
            }else{
                //create alert
                //notify('Rent Application',"$user->name updated form $item->name",'info','fa-cogs');

                //add session message and redirect
                setFlash('success_msg','Form saved successfully');
            }


			return redirect()->route('my.applications');
		}
}
