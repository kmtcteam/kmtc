<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

$router->bind('fieldtypes',function($id){
    return \App\FieldType::whereId($id)->first();
});
$router->bind('forms',function($id){
    return \App\Form::whereId($id)->first();
});

$router->bind('form_id',function($id){
    return \App\Form::whereId($id)->first();
});
$router->bind('service_id',function($id){
    return \App\Service::whereId($id)->first();
});

$router->bind('app_token',function($id){
    return \App\Application::whereToken($id)->first();
});

$router->bind('application_id',function($id){

    return \App\Application::whereId($id)->first();
});


Route::group(['middleware' => ['web']], function () {

	Route::group(['prefix' => 'formmanager','middleware'=>'auth'], function($router) {

		Route::get('/', function() {
			dd('This is the Smstroll module index page.');
		});


		$router->get('fields','FieldTypesController@index')->name('fields.index');
		$router->post('fields/create','FieldTypesController@store')->name('fields.store');
		$router->patch('fields/{contact_id}/update','FieldTypesController@update')->name('fields.update');
		$router->put('fields/{contact_id}/update','FieldTypesController@update')->name('fields.update');
		$router->post('fields/{contact_id}/update','FieldTypesController@update')->name('fields.update');
		$router->delete('fields/{contact_id}/delete','FieldTypesController@destroy')->name('fields.destroy');


		$router->get('forms','FormsController@index')->name('forms.index');
		$router->get('forms/create','FormsController@create')->name('forms.create');
		$router->post('forms/create','FormsController@store')->name('forms.store');
		$router->get('forms/{form_id}','FormsController@show')->name('forms.show');
		$router->get('forms/{form_id}/edit','FormsController@edit')->name('forms.edit');
		$router->patch('forms/{form_id}/update','FormsController@update')->name('forms.update');
		$router->put('forms/{form_id}/update','FormsController@update')->name('forms.update');
		$router->post('forms/{form_id}/update','FormsController@update')->name('forms.update');
		$router->delete('forms/{form_id}/delete','FormsController@destroy')->name('forms.destroy');


		$router->get('services','ServicesController@index')->name('services.index');
		$router->get('services/create','ServicesController@create')->name('services.create');
		$router->post('services/create','ServicesController@store')->name('services.store');
		$router->get('services/{service_id}','ServicesController@show')->name('services.show');
		$router->get('services/{service_id}/edit','ServicesController@edit')->name('services.edit');
		$router->patch('services/{service_id}/update','ServicesController@update')->name('services.update');
		$router->put('services/{service_id}/update','ServicesController@update')->name('services.update');
		$router->post('services/{service_id}/update','ServicesController@update')->name('services.update');
		$router->delete('services/{service_id}/delete','ServicesController@destroy')->name('services.destroy');


		$router->get('applications','ApplicationsController@index')->name('applications.index');
		$router->get('applications/create','ApplicationsController@create')->name('applications.create');
		$router->post('applications/create','ApplicationsController@store')->name('applications.store');
		$router->get('applications/{application_id}','ApplicationsController@show')->name('applications.show');
		$router->get('applications/{application_id}/edit','ApplicationsController@edit')->name('applications.edit');
		$router->patch('applications/{application_id}/update','ApplicationsController@update')->name('applications.update');
		$router->put('applications/{application_id}/update','ApplicationsController@update')->name('applications.update');
		$router->post('applications/{application_id}/update','ApplicationsController@update')->name('applications.update');
		$router->delete('applications/{application_id}/delete','ApplicationsController@destroy')->name('applications.destroy');




		Route::group(['prefix' => 'former'], function($router) {

			  $router->get('apply/{form_id}','FormManagementController@getCreateApplication')->name('get.application');
			  $router->get('fill/{app_token}','FormManagementController@getFillForm')->name('fill.form');
			  $router->get('publish/{app_token}','FormManagementController@publishApplication')->name('publish.application');
			  $router->post('fill/{app_token}','FormManagementController@postSaveFormData')->name('save.form');
			$router->post('add-form-section/{id}','FormManagementController@postAddFormSection')->name('post.addFormSection');
			$router->post('add-form-field/{id}','FormManagementController@postAddFormField')->name('post.addFormField');
			$router->post('save-form','FormManagementController@postSaveFormData')->name('post.saveFormData');
			$router->post('publish-form/{form_id}','FormManagementController@publishForm')->name('publish.form');
		});

	});
});
