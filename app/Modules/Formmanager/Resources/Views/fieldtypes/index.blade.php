@extends(Theme::getActive().'::layouts.base')

@section('container')
    <button class="btn btn-primary" data-toggle="modal" data-target="#frmAddModal"><i class="fa fa-plus"></i> Add New</button>

        <h3>Existing Field Types</h3>
        {!!
          $items->columns(array(
              'id' => '#',
              'name' => 'Name'
          ))
          ->attributes(array(
                'id' => 'table-1',
                'class' => 'table table-striped dataTables',
            ))
          ->render()
        !!}

@stop


@push('modals')
  <div class="modal" id="frmAddModal">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Add Estate</h4>
              </div>
              <div class="modal-body">
                  {!! form($form) !!}
              </div>

          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endpush
