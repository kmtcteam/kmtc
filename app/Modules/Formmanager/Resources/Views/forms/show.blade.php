@extends(Theme::getActive().'::layouts.base')

@section('container')

        <div class="panel panel-while">
                <div class="panel-heading">
                        <div class="panel-title">Services</div>
                </div>
                <div class="panel-body">
                        {!! $form->render() !!}
                </div>
        </div>

@stop
