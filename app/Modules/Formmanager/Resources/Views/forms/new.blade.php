@extends(Theme::getActive().'::layouts.base')

@section('container')
   <div class="innerLR">
        {!! form($form) !!}
   </div>
@stop
