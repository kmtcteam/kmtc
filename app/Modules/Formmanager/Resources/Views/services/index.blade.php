@extends(Theme::getActive().'::layouts.base')

@section('container')
    <div class="panel panel-while">
        <div class="panel-heading">
            <div class="panel-title">Services</div>
        </div>
        <div class="panel-body">
            <button class="btn btn-primary" data-toggle="modal" data-target="#frmAddModal"><i class="fa fa-plus"></i> Create Service </button>
            {!!
              $items->columns(array(
                  'id' => 'Service ID',
                  'code'=>'Service Code',
                  'name' => 'Name',
                  'description' => 'Description',
                  'user_id'=>'Created By',
                  'actions'=>'Action(s)'
              ))
              ->means('user_id','author.name')
               ->modify('actions', function($item) {
                    return '
                    <a href="'.route('services.show',$item->id).'" class="btn btn-primary btn-xs"> <i class="fa fa-folder"></i> view </a>
                    <a href="'.route('services.edit',$item->id).'" class="btn btn-info btn-xs"> <i class="fa fa-pencil"></i> edit</a>
                    ';
                })
                ->attributes(array(
                    'id' => 'table-1',
                    'class' => 'table table-striped dataTables',
                ))
              ->showPages()
              ->render()
            !!}
        </div>
    </div>


@stop

@push('modals')
  <div class="modal" id="frmAddModal">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Add Service</h4>
              </div>
              <div class="modal-body">
                  <form action="{{route('services.store')}}" method="post">
                      {!! csrf_field() !!}
                      @include('formmanager::services._form')
                      <div class="modal-footer">
                          <button type="submit" class="btn btn-primary"> Add Service</button>
                      </div>
                  </form>
              </div>

          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endpush
