<div class="form-group {{$errors->has('name')? 'has-error': ''}}">
    {!! Form::label('name','Name:') !!}
    {!! Form::text('name',null,['class'=>'form-control']) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{$errors->has('code')? 'has-error': ''}}">
    {!! Form::label('code','Code:') !!}
    {!! Form::text('code',null,['class'=>'form-control code']) !!}
    {!! $errors->first('code', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{$errors->has('limits')? 'has-error': ''}}">
    {!! Form::label('limits','No. of Applications Per User (0 for undefined):') !!}
    {!! Form::number('limits',0,['class'=>'form-control']) !!}
    {!! $errors->first('limits', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{$errors->has('description')? 'has-error': ''}}">
    {!! Form::label('description','Description') !!}
    {!! Form::textarea('description',null,['class'=>'form-control code']) !!}
    {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
</div>

<input type="hidden" name="user_id" value="{{user()->id}}">


