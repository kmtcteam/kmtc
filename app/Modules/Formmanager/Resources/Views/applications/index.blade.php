@extends(Theme::getActive().'::layouts.base')

@section('container')
    <div class="panel panel-while">
        <div class="panel-heading">
            <div class="panel-title">Applications</div>
        </div>
        <div class="panel-body">
            {!!
      $items->columns(array(
          'application_no'=>'Application No',
          'name' => 'Customer',
          'service'=>'Service',
          'status_id'=>'Approval Status',
          'created_by'=>'Created By',
          'actions'=>'Action(s)'
      ))
      ->means('created_by','author.name')
      ->modify('name',function($item) {
            return $item->customer.
           '<br>
           <small>Created:'.$item->created_at.'
           ';
      })->modify('service',function($item) {
            return $item->applicable;
      })
      ->modify('status_id',function($item) {

                return dynaStatus($item->status_id);

      })
       ->modify('actions', function($item) {
            return '
            <a  href="'.route('applications.show',$item->id).'" class="btn btn-info btn-xs"> <i class="fa fa-folder"></i> view </a>

            ';
        })
        ->attributes(array(
        'id' => 'table-1',
        'class' => 'table table-striped table-hover p-table dataTables',
        ))
      ->showPages()
      ->render()
    !!}
        </div>
    </div>


    <div class="modal" id="frmAddModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Create Application</h4>
                </div>
                <div class="modal-body">

                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop

@section('javascripts')
    <script>

    </script>
@endsection