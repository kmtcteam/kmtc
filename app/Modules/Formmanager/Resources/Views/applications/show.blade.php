@extends(Theme::getActive().'::layouts.base')

@section('container')
    <div class="panel">
        <div class="panel-body">
            {!! $appView->renderApplication() !!}
            <div class="modal-footer">
                @if ($item->status_id != 1)
                    <img src=" {!! getStatusStamp($item->status_id)!!}">


                    <form>
                        <label for="status_id">Change Status to:</label>
                        <select id="status_id" name="status_id">
                            <option value="1">Pending</option>
                            <option value="2">Approved</option>
                            <option value="3">Review</option>
                            <option value="4">Reject</option>
                        </select>
                        <button class="btn btn-primary"><i class="fa fa-check-o"></i> Update </button>
                    </form>
               @endif
            </div>
        </div>
    </div>
    <div class="modal" id="frmApproveModal">

        {!! Form::open(['route'=>['applications.update',$item->id],'method'=>'PATCH']) !!}
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Application Approval</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <label>Start Date:</label>
                            <input type="date" name="start" class="form-control">
                        </div>

                        <input type="hidden" name="status_id" value="2">

                        <div class="form-group">
                            <label>First Invoice Due On:</label>
                            <input type="date" name="next_due" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="billing_period">Billing Cycle:</label>
                            <select id="billing_period" name="billing_period" class="form-control">
                                <option value="1">Monthly</option>
                                <option value="3">Quartely</option>
                                <option value="4">Triannual</option>
                                <option value="6">Bi-annual</option>
                                <option value="12">Yearly</option>
                            </select>
                        </div>

                    <div class="form-group">
                        <label><input type="checkbox" name="print_invoice" value="1" class="">Generate Invoice Now?:</label>

                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Application</button>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
        {!! Form::close() !!}
    </div><!-- /.modal -->


    <div class="modal" id="frmReviewModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Application Review</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route'=>['applications.update',$item->id],'method'=>'PATCH']) !!}
                        <div class="form-group">
                            <label></label>
                        </div>
                    {!! Form::close() !!}
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal" id="frmRejectionModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Application Rejection</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route'=>['applications.update',$item->id],'method'=>'PATCH']) !!}
                        <div class="form-group">
                            <label></label>
                        </div>
                    {!! Form::close() !!}
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop

@section('javascripts')
    <script>
        $(function(){
            $('#status_id').change(function(){
                var status = $(this).val();
                if (status == '2'){
                    $('#frmApproveModal').modal();
                }else if(status == '3'){
                    $('#frmReviewModal').modal();
                }else if(status =='4'){
                    $('#frmRejectionModal').modal();
                }
            });
        });
    </script>
@endsection