<?php

return [
    /**
     * Specify api gateway
     * {AfricasTalkingGateway,mFollo}
     */
    'api_gateway'=>'AfricasTalkingGateway',

    /**
     * specify your api key
     */
    'api_key'=>'YourApiKey',

    /**
     * Specify your api username
     */

    'api_username'=>'YourApiUsername',

    /**
     * Base URL
     * required for mFollo
     */
    'mfollo_base_url'=>null,

    /**
     * Sms source
     * Required for mFollo
     */
    'mfollo_source_no'=>null,
];