<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

$router->bind('template_id',function($id){
	return \App\Template::whereId($id)->first();
});

Route::group(['middleware' => ['web']], function () {
	Route::group(['prefix' => 'sendsms'], function($router) {

		$router->get('templates','TemplatesController@index')->name('templates.index');
		$router->get('templates/create','TemplatesController@create')->name('templates.create');
		$router->get('templates/{template_id}','TemplatesController@show')->name('templates.show');
		$router->get('templates/{template_id}/edit','TemplatesController@edit')->name('templates.edit');
		$router->post('templates/create','TemplatesController@store')->name('templates.store');
		$router->patch('templates/{template_id}/update','TemplatesController@update')->name('templates.update');
		$router->put('templates/{template_id}/update','TemplatesController@update')->name('templates.update');
		$router->post('templates/{template_id}/update','TemplatesController@update')->name('templates.update');
		$router->delete('templates/{template_id}/delete','TemplatesController@destroy')->name('templates.destroy');


	});
});