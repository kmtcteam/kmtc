<?php
namespace App\Modules\Sendsms\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class SendsmsServiceProvider extends ServiceProvider
{
	/**
	 * Register the SendSms module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Sendsms\Providers\RouteServiceProvider');
		$configPath = realpath(dirname(__FILE__) .'/../Config/sms.php');
		$this->mergeConfigFrom($configPath, 'sms.php');

		$this->app->bind('SMS',function(){
			$guard = auth()->guard();
			//dd($guard);
			return new \App\Modules\Sendsms\LaravelSMS($guard);
		});

		$this->registerNamespaces();
	}

	/**
	 * Register the SendSms module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('sendsms', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('sendsms', base_path('resources/views/vendor/sendsms'));
		View::addNamespace('sendsms', realpath(__DIR__.'/../Resources/Views'));
	}

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->publishes([
				realpath(dirname(__FILE__) .'/../Config/sms.php') => config_path('sms.php'),
				realpath(dirname(__FILE__) .'/../Libs/AfricasTalkingGateway.php')=>app_path('/libs/AfricasTalkingGateway.php')
		]);
	}
}
