<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 17:50
 */

namespace App\Modules\Sendsms\Providers;

use Illuminate\Support\ServiceProvider;
class LaravelDustServiceProvider extends  ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DUST',function(){
            return new \Mitajunior\LaravelDust\LaravelDust;
        });
    }
}