<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 17:53
 */

namespace App\Modules\Sendsms;


class LaravelDust
{
    protected $data;

    protected $html;

    protected $template;

    protected $engine;

    public function __construct($data=[],$html)
    {
        $this->data = $data;
        $this->html = $html;
        $this->engine = new \Dust\Dust();
        return $this;
    }

    public function render()
    {
        $this->template = $this->engine->compile($this->html);
        return $this->engine->renderTemplate($this->template,$this->data);
    }
}