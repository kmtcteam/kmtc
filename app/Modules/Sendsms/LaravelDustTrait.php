<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 18:58
 */

namespace App\Modules\Sendsms;;


trait LaravelDustTrait
{
    protected $html;
    protected $sms;
    protected $data;

    public function getData()
    {
        return $this->data;
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function getSms()
    {
        return $this->sms;
    }

    public function renderHtml(array $data = [])
    {
        $this->data = $data;
        $this->setHtml();
        $dust = new LaravelDust($this->data,$this->getHtml());

        return $dust->render();
    }

    public function renderSMS(array $data = [])
    {
        $this->data = $data;
        $this->setSms($this->description);
        $dust = new LaravelDust($this->data,$this->getSms());

        return $dust->render();
    }

    public function sendEmail($receiver,$data = [])
    {
        $template  = $this;

        $data['body'] = $this->renderHtml($data);
        \Mail::queue('emails.blank',$data,function($m) use ($receiver,$template){
            $m->to($receiver)
            ->subject($template->getSubject());
        });

        return true;

    }
}