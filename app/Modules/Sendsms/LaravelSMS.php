<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 12:49
 */

namespace App\Modules\Sendsms;


class LaravelSMS implements LaravelSMSInterface
{
    protected $gateway = null;
    protected $api_key = null;
    protected $api_username = null;
    protected $recipients = null;
    protected $mfollo_base_url = null;
    protected $mfollo_source_no = null;
    protected $sms = null;
    protected $pretend;
    public function __construct($pretend = false)
    {
        $this->gateway = config('laravel-sms.api_gateway','AfricasTalkingGateway');
        $this->api_key =config('laravel-sms.api_key','YourApiKey');
        $this->api_username = config('laravel-sms.api_username','YourApiUsername');
        $this->mfollo_base_url = config('laravel-sms.mfollo_base_url','mfollo_base_url');
        $this->mfollo_source_no = config('laravel-sms.mfollo_source_no','mfollo_source_no');
        $this->pretend = $pretend;
        //dd($this->api_username);
        if ($this->gateway =='AfricasTalkingGateway')
            return $this->sms = new AfricasTalkingSMS($this->api_username,$this->api_key,$pretend);
        if ($this->gateway = 'mFollo')
            return $this->sms =  new MFolloSMS($this->mfollo_base_url,$this->mfollo_source_no,$pretend);

        else
            return 'Unknown gateway';
    }

    /**
     * Send the SMS message
     *
     * @param  string $to comma-separated list of mobile numbers
     * @param  String $message The SMS message
     * @return boolean
     */
    public function send($to, $message)
    {
        if($this->pretend)
            return true;

        try
        {
            $results = $this->sms->send($to, $message);
            if ($results)
                return 'successful';

            return 'failed';
        }
        catch(\Exception $e)
        {
            \Log::error("Gateway says" . $e->getMessage());
            return false;
        }
    }

}