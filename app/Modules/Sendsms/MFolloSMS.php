<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 02/12/2015
 * Time: 14:24
 */

namespace App\Modules\Sendsms;


class MFolloSMS implements LaravelSMSInterface
{
    /**
     * Whether to pretend to send an SMS
     */
    protected $pretend;

    /**
     * @var string endpoint to make the request against
     */
    protected $base_url;

    /**
     * @var SMS Source address
     */
    protected $source;

    /**
     * Create a new filesystem storage instance
     *
     * @param string $base_url
     * @param string $source
     * @param boolean $pretend Whether to always return true from send without attempting to send
     */
    public function __construct($base_url, $source, $pretend = false)
    {
        $this->base_url = $base_url;
        $this->source = $source;
        $this->pretend = $pretend;
    }

    /**
     * Send the SMS message
     *
     * @param  string $to comma-separated list of mobile numbers
     * @param  String $message The SMS message
     * @return boolean
     */
    public function send($to, $message)
    {

        // remove preceding plus if it exists
        $to = preg_replace('/^\+/', '', $to);

        // check and log the number if it doesnt match our format 254720417691
        if(!preg_match('/^\d+$/', $to))
        {
            \Log::error("Invalid mobile number: " . $to);
            return false;
        }

        // build params
        $url = sprintf('%s?%s', $this->base_url, http_build_query(['MSISDN' => $to, 'MESSAGE' => $message, 'SOURCE' => $this->source]));
        \Log::info($url);

        if($this->pretend)
            return true;

        try
        {
            $response = \Httpful\Request::get($url)->send();;
            \Log::info($response);
            return $response->code == 200 && !!$response->body;
        }
        catch(\Exception $e)
        {
            \Log::error("MFollo SMS Error: with message " . $e->getMessage());
            return false;
        }
    }
}