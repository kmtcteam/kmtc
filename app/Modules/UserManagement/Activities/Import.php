<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/19/2016
 * Time: 1:49 AM
 */

namespace App\Modules\UserManagement\Activities;


class Import
{
    protected $file;

    protected $importRoles;

    public function __construct($file)
    {
        $this->file;
    }

    public function importRoles()
    {
        $importRoles = new ImportRoles();
        $importRoles->setFile($this->file);
        $importRoles->import();
    }
}