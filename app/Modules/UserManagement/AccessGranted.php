<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/18/2016
 * Time: 11:21 PM
 */

namespace App\Modules\UserManagement;

use App\Role;
use Illuminate\Contracts\Auth\Guard;
class AccessGranted
{
    /**
     * @var Illuminate\Contracts\Auth\Guard
     */
    protected $auth;

    /**
     * Create a new UserHasPermission instance.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Checks if user has the given permissions.
     *
     * @param array|string $permissions
     *
     * @return bool
     */
    public function can($permissions)
    {
        if ($this->auth->check()) {
            return $this->auth->user()->can_do($permissions);
        }

        return false;
    }

    /**
     * Checks if user has at least one of the given permissions.
     *
     * @param array $permissions
     *
     * @return bool
     */
    public function canAtLeast($permissions)
    {
        if ($this->auth->check()) {
            return $this->auth->user()->canAtLeast($permissions);
        }

        return false;
    }

    /**
     * Checks if user is assigned the given role.
     *
     * @param  string $slug
     * @return bool
     */
    public function is($role)
    {
        if ($this->auth->check()) {
            return $this->auth->user()->is($role);
        }

        return false;
    }

    /**
     * Checks if user is assigned the given role.
     *
     * @param  string $slug
     * @return bool
     */
    public function has($role)
    {
        if ($this->auth->check()) {
            return $this->auth->user()->has($role);
        }

        return false;
    }


    /**
     * Checks if user has the given permissions.
     *
     * @param array|string $permissions
     *
     * @return bool
     */
    public function ofGroup($group)
    {
        if ($this->auth->check()) {
            return $this->auth->user()->ofGroup($group);
        }

        return false;
    }

    public function getRoleByName($name)
    {
        return Role::whereName($name)->first();
    }

}