<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/18/2016
 * Time: 11:11 PM
 */

namespace App\Modules\UserManagement\Traits;


trait AccessGrantedUserTrait
{
    /*
        |----------------------------------------------------------------------
        | Role Trait Methods
        |----------------------------------------------------------------------
        |
        */



    /**
     * Get all user roles.
     *
     * @return array|null
     */
    public function getRoles()
    {
        if (! is_null($this->roles)) {
            return $this->roles->pluck('slug')->all();
        }

        return null;
    }

    /**
     * Checks if the user has the given role.
     *
     * @param  string $slug
     * @return bool
     */
    public function is($slug)
    {

        $slug = strtolower($slug);

        foreach ($this->roles as $role) {
            if ($role->slug == $slug) return true;
        }


        return false;
    }

    /**
     * Checks if the user has the given role.
     *
     * @param  string $slug
     * @return bool
     */
    public function has($slug)
    {
        $slug = strtolower($slug);

        foreach ($this->roles as $role) {
            //dd($role->category);
            if (strtolower($role->id) == $slug) return true;
        }

        foreach ($this->roles as $role) {
            //dd($role->category);
            if (strtolower($role->slug) == $slug) return true;
        }

        foreach ($this->roles as $role) {
            //dd($role->category);
            if (strtolower($role->category) == $slug) return true;
        }



        return false;
    }

    /**
     * Assigns the given role to the user.
     *
     * @param  int $roleId
     * @return bool
     */
    public function assignRole($roleId = null)
    {
        $roles = $this->roles;

        if (! $roles->contains($roleId)) {
            return $this->roles()->attach($roleId);
        }

        return false;
    }

    /**
     * Revokes the given role from the user.
     *
     * @param  int $roleId
     * @return bool
     */
    public function revokeRole($roleId = '')
    {
        return $this->roles()->detach($roleId);
    }

    /**
     * Syncs the given role(s) with the user.
     *
     * @param  array $roleIds
     * @return bool
     */
    public function syncRoles(array $roleIds)
    {
        return $this->roles()->sync($roleIds);
    }

    /**
     * Revokes all roles from the user.
     *
     * @return bool
     */
    public function revokeAllRoles()
    {
        return $this->roles()->detach();
    }

    /*
    |----------------------------------------------------------------------
    | Permission Trait Methods
    |----------------------------------------------------------------------
    |
    */

    /**
     * Get all user role permissions.
     *
     * @return array|null
     */
    public function getPermissions()
    {
        $permissions = [[], []];

        foreach ($this->roles as $role) {
            $permissions[] = $role->getPermissions();
        }

        return call_user_func_array('array_merge', $permissions);
    }

    /**
     * Check if user has the given permission.
     *
     * @param  string $permission
     * @return bool
     */
    public function can_do($permission)
    {
        $can = false;

        foreach ($this->roles as $role) {
            //dd($this->roles);
            if ($role->special === 'all-access') {
                return true;
            }

            if ($role->special === 'no-access') {
                return false;
            }

            if ($role->can($permission)) {
                $can = true;
            }
        }

        return $can;
    }

    /**
     * Check if user has at least one of the given permissions
     *
     * @param  array $permissions
     * @return bool
     */
    public function canAtLeast(array $permissions)
    {
        $can = false;

        foreach ($this->roles as $role) {
            if ($role->special === 'all-access') {
                return true;
            }

            if ($role->special === 'no-access') {
                return false;
            }

            if ($role->canAtLeast($permissions)) {
                $can = true;
            }
        }

        return $can;
    }


    /**
     * Check if user has the given group
     *
     * @param  string $permission
     * @return bool
     */
    public function ofGroup($group)
    {
        $can = false;

        foreach ($this->roles as $role) {
            //dd($this->roles);
            if ($role->special === 'all-access') {
                return true;
            }

            if ($role->special === 'no-access') {
                return false;
            }

            if ($role->ofGroup($group)) {
                $can = true;
            }
        }

        return $can;
    }

    /*
    |----------------------------------------------------------------------
    | Magic Methods
    |----------------------------------------------------------------------
    |
    */

    /**
     * Magic __call method to handle dynamic methods.
     *
     * @param  string $method
     * @param  array  $arguments
     * @return mixed
     */
    public function __call($method, $arguments = array())
    {
        // Handle isRoleslug() methods
        if (starts_with($method, 'is') and $method !== 'is') {
            $role = substr($method, 2);

            return $this->is($role);
        }

        // Handle canDoSomething() methods
        if (starts_with($method, 'can_do') and $method !== 'can_do') {
            $permission = substr($method, 3);

            return $this->can_do($permission);
        }

        return parent::__call($method, $arguments);
    }
}