<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/18/2016
 * Time: 11:12 PM
 */

namespace App\Modules\UserManagement\Traits;


trait AccessGrantedRoleTrait
{
    /**
     * Get permission slugs assigned to role.
     *
     * @return array
     */
    public function getPermissions()
    {
        return $this->permissions->pluck('slug')->all();
    }

    /**
     * Checks if the role has the given permission.
     *
     * @param  string $permission
     * @return bool
     */
    public function can($permission)
    {
        if ($this->special === 'all-access') {
            return true;
        }

        if ($this->special === 'no-access') {
            return false;
        }

        $permissions = $this->getPermissions();

        if (is_array($permission)) {
            $permissionCount    = count($permission);
            $intersection       = array_intersect($permissions, $permission);
            $intersectionCount  = count($intersection);

            return ($permissionCount == $intersectionCount) ? true : false;
        } else {
            return in_array($permission, $permissions);
        }
    }

    /**
     * Checks if role is of a given group.
     *
     * @param  string $permission
     * @return bool
     */
    public function ofGroup($group)
    {

        if ($this->special === 'all-access') {
            return true;
        }

        if ($this->special === 'no-access') {
            return false;
        }

        $groups = is_array($group) ? $group : array($group);

        return in_array($this->category,$groups);
    }

    /**
     * Check if the role has at least one of the given permissions
     *
     * @param  array $permission
     * @return bool
     */
    public function canAtLeast(array $permission = array())
    {
        if ($this->special === 'all-access') {
            return true;
        }

        if ($this->special === 'no-access') {
            return false;
        }

        $permissions = $this->getPermissions();

        $intersection       = array_intersect($permissions, $permission);
        $intersectionCount  = count($intersection);

        return ($intersectionCount > 0) ? true : false;
    }

    /**
     * Assigns the given permission to the role.
     *
     * @param  int $permissionId
     * @return bool
     */
    public function assignPermission($permissionId = null)
    {
        $permissions = $this->permissions;

        if (! $permissions->contains($permissionId)) {
            return $this->permissions()->attach($permissionId);
        }

        return false;
    }

    /**
     * Revokes the given permission from the role.
     *
     * @param  int $permissionId
     * @return bool
     */
    public function revokePermission($permissionId = '')
    {
        return $this->permissions()->detach($permissionId);
    }

    /**
     * Syncs the given permission(s) with the role.
     *
     * @param  array $permissionIds
     * @return bool
     */
    public function syncPermissions(array $permissionIds = array())
    {
        return $this->permissions()->sync($permissionIds);
    }

    /**
     * Revokes all permissions from the role.
     *
     * @return bool
     */
    public function revokeAllPermissions()
    {
        return $this->permissions()->detach();
    }
}