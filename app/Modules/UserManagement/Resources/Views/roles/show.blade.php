@extends(Theme::getActive().'::layouts.base')

@section('container')
    <div class="portlet light bordered">
        <div class="portlet-title tabbable-line">
            <div class="caption caption-md">
                <i class="icon-globe theme-font hide"></i>
                <span class="caption-subject font-blue-madison bold uppercase"> Role Details</span>
            </div>
        </div>
        <div class="portlet-body">

            <div>
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="form-field-1">
                                Role Name
                            </label>
                            <div class="">
                                <strong>{{$item->name}}</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="form-field-1">
                                Role Slug
                            </label>
                            <div class="">
                                <strong>{{$item->slug}}</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="form-field-1">
                                Role Category
                            </label>
                            <div class="">
                                <strong>{{$item->category}}</strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Permissions

                            </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        @if ($item->special == 'all-access')
                            <div class="note note-info">
                                Role has special access (all access)
                            </div>
                        @elseif ($item->special == 'no-access')
                            <div class="note note-danger">
                                Role has special access (no access)
                            </div>
                        @endif
                        <div class="row">
                            @foreach($perms as $perm)
                                <div class="col-md-3 form-group">
                                    <label>
                                        @if ($item->permissions->pluck('id','name')->search($perm->id))
                                            <span class="fa fa-check"></span>
                                        @else
                                            <span class="fa fa-times"></span>
                                        @endif
                                        {{$perm->name}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{route('admin::roles.edit',$item->id)}}" class="btn btn-primary btn-icon"><i class="fa fa-pencil-square-o"></i> Edit</a>
                    <a href="{{route('admin::roles.index')}}" class="btn btn-default btn-icon"><i class="fa fa-times-circle-o"></i> Back</a>
                </div>
            </div>


        </div>
    </div>
@stop



