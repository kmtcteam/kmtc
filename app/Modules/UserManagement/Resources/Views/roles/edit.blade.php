@extends(Theme::getActive().'::layouts.base')

@section('container')


            {!! Form::model($item,['route'=>['admin::roles.update',$item->id],'method'=>'PATCH']) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {{$errors->has('role_name')? 'has-error': ''}}">
                            <label class="control-label" for="form-field-1">
                                Role Name
                            </label>
                            <div class="">
                                {!! Form::text('role_name',$item->name,['class'=>'form-control','id'=>'role_name']) !!}
                                {!! $errors->first('role_name', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{$errors->has('role_slug')? 'has-error': ''}}">
                            <label class="control-label" for="form-field-1">
                                Role Slug
                            </label>
                            <div class="">
                                {!! Form::text('role_slug',$item->slug,['class'=>'form-control slug','readonly'=>'readonly']) !!}
                                {!! $errors->first('role_slug', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{$errors->has('role_category')? 'has-error': ''}}">
                            <label class="control-label" for="form-field-1">
                                Role Category
                            </label>
                            <div class="">
                                {!! Form::select('role_category',['Backend Access'=>'Backend Access','Frontend Access'=>'Frontend Access'],$item->category,['class'=>'form-control']) !!}
                                {!! $errors->first('role_category', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-default ">
                    <div class="panel-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Permissions
                            <label>
                                <input id="selecctall" type="checkbox" class="form-group" name="" value="1" >
                                Select All
                            </label>
                            </span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach($perms as $perm)
                                <div class="col-md-3 form-group">
                                    <label>
                                        <input type="checkbox"  class="form-group perms" name="perms[]" value="{{$perm->id}}"
                                        {{$item->permissions->pluck('id','name')->search($perm->id) ? 'checked' : ''}}
                                        >
                                        {{$perm->name}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="submit" value="" class="btn btn-success btn-icon"><i class="fa fa-check-square-o"></i> Save</button>
                    <a href="{{route('admin::roles.index')}}" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</a>
                </div>
            {!! Form::close() !!}


@stop

@section('page_js')
    <script src="{{asset('assets/global/plugins/slugify/jquery.slugify.js')}}" type="text/javascript"></script>
@endsection



@section('javascripts')
    <script>
        $(function(){
            $('.slug').slugify('#role_name');
        });

        $("#selecctall").change(function(){
            $(".perms").prop('checked', $(this).prop("checked"));
        });


    </script>
@endsection

