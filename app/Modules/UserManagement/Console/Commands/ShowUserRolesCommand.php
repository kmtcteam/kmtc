<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/19/2016
 * Time: 4:00 AM
 */

namespace App\Modules\UserManagement\Console\Commands;


use Illuminate\Console\Command;

class ShowUserRolesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ag:user {--user=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Show roles of a user";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info($this->getDescription());
        $model = config('access-granted.user_model');

        $user = $model::findOrFail($this->option('user'));
        $this->info("Name: ".$user->getName());
        $this->info("Email: ".$user->email);
        $this->info("Roles: ".$user->roles->implode('name',', '));
    }
}