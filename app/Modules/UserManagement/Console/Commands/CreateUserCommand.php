<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/19/2016
 * Time: 12:03 AM
 */

namespace App\Modules\UserManagement\Console\Commands;

use App\Events\AccountCreated;
use App\Events\UserHasRegistered;
use Illuminate\Console\Command;
class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ag:users:create {--active}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = config('access-granted.user_model');
        $rl = config('access-granted.role_model');
        $roles = $rl::all()->pluck('name','id')->toArray();

        if (!count($roles)){
            $this->error("No roles defined, please add roles first ag:role:create");
            return true;
        }

        $continue = 'y';
        while (in_array($continue,['Yes','Y','y','yes','YES'])){
            $available = collect($model::all(['email'])->toArray());

            $name = $this->ask("Name?");
            $email = $this->ask("Email?");
            while(in_array($email,$available->flatten()->toArray())){
                $email = $this->ask("Already taken. Try another another");
            }
            $password = $this->ask("Password:",generateRandomString(6));
            $phone = $this->ask("Phone");
            $role_id = $this->choice('Role:',$roles);

            $r_choice = \AG::getRoleByName($role_id);

            $credentials = [
                'name'=>$name,
                'email'=>$email,
                'phone'=>$phone,
                'password'=>\Hash::make($password),
                'role_id'=>$r_choice->id,
            ];

            $user = $model::create($credentials);

            $user->roles()->attach($r_choice->id);


            //send email with activation link to the user
            if ($this->option('active')){
                $user->activate();
            }else{
                $user->setActivateRequest();
            }


            event(new AccountCreated($user));

            $continue = $this->ask('Add another use ? [y|n] ','n');
        }
    }

}