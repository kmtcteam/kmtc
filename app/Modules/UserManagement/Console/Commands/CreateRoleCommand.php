<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/19/2016
 * Time: 12:03 AM
 */

namespace App\Modules\UserManagement\Console\Commands;

use Illuminate\Console\Command;
class CreateRoleCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ag:roles:create {--group=Frontend Access} {--all-access} {--no-access}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = config('access-granted.role_model');

        $continue = 'y';
        while (in_array($continue,['Yes','Y','y','yes','YES'])){
            $available = collect($model::all(['slug'])->toArray());

            $name = $this->ask("Name?");
            $slug = $this->ask("Slug");
            while(in_array($slug,$available->flatten()->toArray())){
                $slug = $this->ask("Already taken. Slug");
            }
            $description = $this->ask("Description []",'  ');


            if ($this->option('all-access')){
                $special = 'all-access';
            }elseif($this->option('no-access')){
                $special = 'no-access';
            }else{
                $special = null;
            }

            $model::create([
                'name'=>$name,'slug'=>$slug,
                'description'=>$description,
                'category'=>$this->option('group'),
                'special'=>$special
            ]);

            $continue = $this->ask('Add another role? [y|n] ','n');
        }
    }
}