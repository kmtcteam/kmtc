<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/19/2016
 * Time: 12:02 AM
 */

namespace App\Modules\UserManagement\Console\Commands;

use Illuminate\Console\Command;
class ListRolesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ag:roles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends reminders to tenant to pay the bills and checks is its due for penalties';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['ID','Name','Slug','Group','Special'];

        $model = config('access-granted.role_model');
        $roles = $model::all(['id','name','slug','category','special'])->toArray();


        $this->table($headers,$roles);
    }
}