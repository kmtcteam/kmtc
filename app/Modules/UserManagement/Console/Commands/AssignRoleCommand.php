<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 1/19/2016
 * Time: 12:02 AM
 */

namespace App\Modules\UserManagement\Console\Commands;

use Illuminate\Console\Command;
class AssignRoleCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ag:assign {--user=} {--role=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info($this->getDescription());
        $model = config('access-granted.user_model');
        $rl = config('access-granted.role_model');

        $user = $model::findOrFail($this->option('user'));
        $role = $rl::findOrFail($this->option('role'));

        if ($this->confirm("Are you sure you want assign role $role to $user ? ")){
            $user->roles()->attach($role->id);
        }

    }
}