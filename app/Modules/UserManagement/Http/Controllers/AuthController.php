<?php
namespace App\Modules\UserManagement\Http\Controllers;

use App\Events\AccountCreated;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Caffeinated\Themes\Facades\Theme;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash,Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Exception\NotFoundException;

class AuthController extends Controller
{
    protected $afterLogout = '/';
    protected $afterLogin = '/account';
    protected $afterRegister = '/backend';
    protected $adminUrl = '/backend';


    public function __construct()
    {
        $this->adminUrl = config('access-granted.admin_url');
    }


    public function getLogin()
    {

        return Theme::view('usermanagement::auth.login');
    }

    public function getBackendLogin()
    {

        return Theme::view('usermanagement::auth.login');
    }


    public function postLogin(Request $request)
    {
        //allow user to login with username or email, this will check if the identity provided is an email
        //else it is a username..un comment this line to use either username or password..ensure username field exists in you table
        /*$identity = filter_var($request->get('identity'), FILTER_VALIDATE_EMAIL)? 'email' : 'username';
        $rules = [
            'identity'=>"required|exists:users,$identity",
            'password'=>'required'
        ];  */


        $rules = [
            'email'=>"required|exists:users,email",
            'password'=>'required'
        ];
        $messages = [
            'email.exists'=>'Not found! Please register'
        ];


        $valid = Validator::make($request->all(),$rules,$messages);
        if ($valid->fails()){
            setFlash('error_msg','Invalid input');
            return redirect()->back()
                ->withErrors($valid)
                ->withInput($request->except('password'));
        }

        $credentials = [
            'email' => $request->get('email'),
            'password'=> $request->get('password'),
        ];

        //dd($credentials);

        if (Auth::attempt($credentials)){
            if (!user()->confirmed){
                Auth::logout();
                setFlash('error_msg','Your account has not been activated');
                return redirect()->back();
            }

            if (\AG::ofGroup('Backend Access')){
                return redirect()->intended(property_exists($this,'adminUrl')?$this->adminUrl:'/admin');

            }else{

              //dd(user());
                //return redirect()->intended(property_exists($this,'adminUrl')?$this->adminUrl:'/admin');
                return redirect()->intended(property_exists($this,'afterLogin')?$this->afterLogin:'/account');
            }
        }

        Session::flash('error_msg','Invalid login credentials');
        return redirect()->back();
    }

    public function getRegister()
    {
        return Theme::view('usermanagement::auth.register');
        //return view('auth.register');
    }


    public function sign_on(Request $request)
    {

        // verify the signature
        $data = json_decode(base64_decode($request->get('data')), true);
        $is_valid = true;

        // check our expected keys
        if($data === null || !array_key_exists('email', $data) || !array_key_exists('id_number', $data) || !array_key_exists('at', $data) || !array_key_exists('signature', $data))
            $is_valid = false;

        // validate signature
        $signature = $data['signature'];
        unset($data['signature']);

        // make hash of the data
        $computed_signature = base64_encode(hash_hmac('sha256', json_encode($data), config('app.client_secret'), true));

        if($signature !== $computed_signature)
            $is_valid = false;

        if(!$is_valid)
            return response("Invalid data", 400);
        else {
            $account = User::whereEmail($data['email'])->first();
            if(!$account){
                $me = [
                    'first_name'=>$data['first_name'],
                    'last_name'=>$data['last_ame'],
                    'id_number'=>$data['id_number'],
                    'phone'=>$data['mobile_number'],
                    'email'=>$data['email'],
                    'password'=>''
                ];

                $account = $this->createAccount($me);
            }

            if (Auth::loginUsingId($account->id)){
                return redirect()->intended($this->afterLogin);
            }

        }

        return response('login failed',400);
    }

    public function postRegister(Request $request)
    {
        //dd($request->all());
        $rules = [
            'name'=>'required',
            'phone'=>'required|min:8|numeric',
            //'id_number'=>'required|unique:users,id_number',
            'email'=>'required|unique:users,email',
            'password'=>'required|min:6|confirmed',
            'password_confirmation'=>'required|same:password',
        ];

        $valid = Validator::make($request->all(),$rules);
        if ($valid->fails()){
            return redirect()->back()
                ->withErrors($valid)
                ->withInput($request->except(['password','password_confirmation']));
        }

        $credentials = [
            'name'=>$request->get('name'),
            'email'=>$request->get('email'),
            'phone'=>encode_phone_number($request->get('phone')),
            'password'=>Hash::make($request->get('password')),
            'role_id'=>2,
        ];

        $user = User::create($credentials);

        $user->assignRole($user->role_id);

        //send email with activation link to the user
        $user->setActivateRequest();

        event(new AccountCreated($user));

        //redirect back
        setFlash('success_msg','Account created successfully, verification link has been sent to your email');
        return redirect()->route('get.login');

    }



    public function getActivateAccount($token)
    {
        $user = User::whereConfirmToken($token)->first();
        if (!$user){
            setFlash('error_msg','Invalid request');
            return redirect()->route('get.login');
        }
        $user->confirm_token = null;
        $user->confirmed = 1;
        $user->active = 1;
        $user->email_verified = 1;
        $user->save();

        setFlash('success_msg','Your account has been successfully activated, login to your account');
        return redirect()->route('get.login');
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->to(property_exists($this,'afterLogout')?$this->afterLogout:'/');
    }


    public function getRequestValidation()
    {
        return Theme::view('auth.code');
    }




    public function getForgot()
    {
        return Theme::view('auth.forgot');
    }

    public function postForgotRequest(Request $request)
    {
        $this->validate($request,[
            'email'=>'required|exists:users,email'
        ]);

        $user = User::whereEmail($request->get('email'))->firstOrFail();

        $user->resetPasswordRequest();

        setFlash('success_msg','Password reset link has been sent to you email');
        return redirect()->route('get.login');

    }

    public function getForgotReset($reset_token)
    {
        $reset = DB::table('password_resets')->where('token',$reset_token)->first();
        if (!$reset){
            setFlash('error_msg','Invalid request');
            redirect()->route('get.login');
        }
        $user = User::whereEmail($reset->email)->firstOrFail();

        return Theme::view('auth.reset',compact('reset_token'));
    }

    public function postForgotReset(Request $request,$token)
    {
        $reset = \DB::table('password_resets')->where('token',$token)->first();
        if (!$reset){
            setFlash('error_msg','Invalid request');
            redirect()->route('get.login');
        }


        $this->validate($request,[
            'password'=>'required|confirmed|min:6'
        ]);
        $user = User::whereEmail($reset->email)->firstOrFail();

        $user->password = Hash::make($request->get('password'));
        $user->save();

        //delete reset request
        \DB::table('password_resets')->where('token',$token)->delete();

        setFlash('success_msg','Your password was successfully reset');
        return redirect()->route('get.login');

    }
}
