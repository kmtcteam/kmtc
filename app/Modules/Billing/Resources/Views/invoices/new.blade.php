@extends('targetadmin::layouts.base')

@section('container')
    {!! Form::open(['route'=>'services.store','method'=>'post','files'=>true]) !!}
    @include('smstroll::services._form')
    <div class="form-group">
        {!! Form::submit('Create Service',['class'=>'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}

@stop

@push('javascripts')
<script>
    $(function(){
        $('#timepicket1').timepicker({ template: 'modal' })
        $('#datetimepicker').datepicker ({
            format: 'yyyy/mm/dd'
        })
    });
</script>
@endpush
