<div class="row">
    <div class="col-md-8">
        <div class="form-group {{$errors->has('name')? 'has-error': ''}}">
            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',null,['class'=>'form-control']) !!}
            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{$errors->has('description')? 'has-error': ''}}">
            {!! Form::label('description','Description:') !!}
            {!! Form::textarea('description',null,['class'=>'form-control textarea','id'=>'count-textarea1']) !!}
            {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-md-4">

        <div class="form-group {{$errors->has('period')? 'has-error': ''}}">
            {!! Form::label('period','Repeating Service?:') !!}
            {!! Form::checkbox('period',1,['class'=>'form-control']) !!}
            {!! $errors->first('period', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{$errors->has('cycle_name')? 'has-error': ''}}">
            {!! Form::label('cycle_name','Every:') !!}
            {!! Form::select('cycle_name',['hours'=>'Hour','days'=>'Day','weeks'=>'Week','months'=>'Month','years'=>'Year'],null,['class'=>'form-control']) !!}
            {!! $errors->first('cycle_name', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{$errors->has('cycle_count')? 'has-error': ''}}">
            {!! Form::label('cycle_count','How Many Times?:') !!}
            {!! Form::number('cycle_count',null,['class'=>'form-control','min'=>1,'max'=>24]) !!}
            {!! $errors->first('cycle_count', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group  {{$errors->has('cycle_time')? 'has-error': ''}}">
            {!! Form::label('cycle_time','Time:') !!}
            <div class="input-group bootstrap-timepicker timepicker">
                {!! Form::text('cycle_time',null,['id'=>'timepicket1','class'=>'form-control']) !!}
                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
            </div>

            {!! $errors->first('cycle_time', '<span class="help-block">:message</span>') !!}
        </div>


        <div class="form-group {{$errors->has('start')? 'has-error': ''}}">
            {!! Form::label('start','Starting from:') !!}
            <div class="input-group bootstrap-timepicker timepicker">
                {!! Form::text('start',null,['id'=>'datetimepicker','class'=>'form-control']) !!}
                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
            </div>
            {!! $errors->first('start', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{$errors->has('contacts')? 'has-error': ''}}">
            {!! Form::label('contacts','Select Contact(s) - Press Ctrl to select more than one:') !!}
            {!! Form::select('contacts[]',hasRole(1)? getAllData(\App\Group::class,'id','name') : user()->groups->pluck('name','id')->toArray(),null,['class'=>'form-control','multiple'=>'multiple']) !!}
            {!! $errors->first('contacts', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
</div>


