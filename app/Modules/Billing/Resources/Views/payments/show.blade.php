@extends('targetadmin::layouts.base')


@section('container')
    <div class="row">


        <div class="col-md-8">

            <h3>#INV {{$bill->bill_no}} <small class="pull-right">{{date('M d, Y',strtotime($bill->created_at))}}</small></h3>

            <br>

            <br>



            <div class="row">

                <div class="col-md-6 col-sm-6">

                    <p><strong>From:</strong></p>


                    <table class="table">
                        <tbody>
                        <tr>
                            <td style="width: 90px">
                                <div class="thumbnail">
                                    <img src="{{Theme::asset('img/avatars/avatar-1.jpg')}}" alt="Avatar" style="width: 90px;" />
                                </div>
                            </td>
                            <td>
                                <strong>Jumpstart Inc.</strong> <br />
                                2501 N Green Valley Pkwy, #150 <br />
                                Henderson, NV 89014 <br />
                                Phone: 702-555-7777 <br />
                                Fax: 702 444-6666
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </div> <!-- /.col -->

                <div class="col-md-6 col-sm-6">

                    <p><strong>To:</strong></p>


                    <table class="table">
                        <tbody>
                        <tr>
                            <td style="width: 90px">
                                <div class="thumbnail">
                                    <img src="{{Theme::asset('img/avatars/avatar-2.jpg')}}" alt="Avatar" style="width: 90px;" />
                                </div>
                            </td>
                            <td>
                                <strong>{{$bill->author}}</strong> <br />
                                {{$bill->author->email}} <br />
                                {{$bill->author->phone}}
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </div> <!-- /.col -->


            </div> <!-- /.row -->

            <br>

            <h3>Invoice Details</h3>

            <br>


            <div class="table-responsive">
                <table class="table">

                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>Period</th>
                        <th class="price">Price</th>
                        <th class="total">Total</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>{{$bill->service}} <br>
                            <p class="text-muted">{{$bill->description}}</p>
                        </td>
                        <td>{{$bill->service->getCycle()}}</td>
                        <td>{{number_format($bill->cost,2)}}</td>
                        <td>{{number_format($bill->total,2)}}</td>
                    </tr>

                    <tr>
                        <td colspan="2"></td>
                        <td><strong>Total Due:</strong></td>
                        <td><strong>{{number_format($bill->total,2)}}</strong></td>
                    </tr>
                    </tbody>
                </table>
            </div> <!-- /.table-resonsive -->





        </div> <!-- /.col -->


        <div class="col-md-4 col-sidebar-right">

            <p><a href="javascript:;" class="btn btn-jumbo btn-primary btn-block">Pay Invoice <br /></a></p>

            <p><a href="javascript:;" class="btn btn-lg btn-tertiary btn-block">Close Invoice</a></p>

            <br><br>

            <h4>Invoice Actions</h4>
            <hr />
            <div class="list-group">

                <a href="javascript:;" class="list-group-item">
                    <i class="fa fa-envelope"></i>
                    &nbsp;&nbsp;<strong>Send</strong> Invoice
                </a>

                <a href="javascript:;" class="list-group-item">
                    <i class="fa fa-pencil"></i>
                    &nbsp;&nbsp;<strong>Edit</strong> Invoice
                </a>

                <a href="javascript:;" class="list-group-item">
                    <i class="fa fa-print"></i>
                    &nbsp;&nbsp;<strong>Print</strong> Invoice
                </a>

                <a href="javascript:;" class="list-group-item">
                    <i class="fa fa-copy"></i>
                    &nbsp;&nbsp;<strong>Duplicate</strong> Invoice
                </a>

                <a href="javascript:;" class="list-group-item">
                    <i class="fa fa-times"></i>
                    &nbsp;&nbsp;<strong>Delete</strong> Invoice
                </a>

            </div>


            <br />




        </div> <!-- /.col -->


    </div> <!-- /.row -->
@stop

