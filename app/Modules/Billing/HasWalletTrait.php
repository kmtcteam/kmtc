<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 3/22/2016
 * Time: 2:21 PM
 */

namespace App\Modules\Billing;


use App\Wallet;

trait HasWalletTrait
{
    public function wallet()
    {
        return $this->hasOne(Wallet::class,'id_number');
    }
}