<?php
namespace App\Modules\Billing\Http\Controllers;

use App\Events\PaymentMade;
use App\Events\PaymentReceived;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Mpesa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BillsApiController extends Controller
{


    public function receiveMpesa(Request $request)
    {

        \Log::info("MPESA IPN ".date('Y-m-d H:i:s')." ".$request->fullUrl());
        $valid = Validator::make($request->all(),[
            'id'=>'required',
            'orig'=>'required',
            'mpesa_code'=>'required',
            'customer_id'=>'required',
            'mpesa_acc'=>'required',
            'mpesa_msisdn'=>'required',
            'mpesa_trx_date'=>'required',
            'mpesa_trx_time'=>'required',
            'mpesa_amt'=>'required',
            'mpesa_sender'=>'required',
            'tstamp'=>'required',
            'business_number'=>'required',
            'routemethod_id'=>'required',
            'routemethod_name'=>'required',
            'text'=>'required',
            'dest'=>'required',
        ]);

        if($valid->fails()){
            echo json_encode([
                'success'=>0,
                'errors'=>$valid->errors(),
                'message'=>'Invalid request parameters'
            ]);

            return;

        }

        //dd(http_build_query($_REQUEST));
        $data = [
            'msg_id'=>$request->get('id'),
            'origin'=>$request->get('orig'),
            'code'=>$request->get('mpesa_code'),
            'customer_id'=>$request->get('customer_id'),
            'account'=>$request->get('mpesa_acc'),
            'msisdn'=>$request->get('mpesa_msisdn'),
            'mid'=>$request->get('id'),
            'txn_date'=>$request->get('mpesa_trx_date'),
            'txn_time'=>$request->get('mpesa_trx_time'),
            'amount'=>$request->get('mpesa_amt'),
            'sender'=>$request->get('mpesa_sender'),
            'txn_stamp'=>$request->get('tstamp'),
            'biz_no'=>$request->get('business_number'),
            'route_method_id'=>$request->get('routemethod_id'),
            'route_method_name'=>$request->get('routemethod_name'),
            'notes'=>$request->get('text'),
            'destination'=>$request->get('dest'),
        ];


        $mpesa  = Mpesa::create($data);

        if ($mpesa){
            event(new PaymentReceived($mpesa));
        }

        echo json_encode([
            'success'=>1,
            'item'=>$mpesa->toJson(),
            'message'=>'Received'
        ]);

        return;
    }

    public function verifyMpesa(Request $request,$bill)
    {
        $valid = Validator::make($request->all(),[
            'account'=>'required|exists:bills,'
        ]);

    }
}
