<?php
namespace App\Modules\Billing\Http\Controllers;

use App\Events\PaymentMade;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Exception\NotFoundException;
use Omnipay\Omnipay;

class PaypalController extends Controller
{
    private $data;


    public function makePayment(Request $request)
    {
        $this->validate($request,[
            'amount'=>'required|numeric|min:1',
            'description'=>'required',
            'name'=>'required',
            'cancelUrl'=>'required|url',
            'returnUrl'=>'required|url',
            'completeUrl'=>'required|url',
            'payable_id'=>'required|int',
            'payable_type'=>'required|model'
        ]);

        $class = $request->get('payable_type');
        $id = $request->get('payable_id');
        $payable = $class::findOrFail($id);
        $receipt  = generatePaymentRequest($payable,['currency'=>$request->get('currency')]);

        $params = $request->all();
        $params = array_add($params,'receipt',$receipt);

        \Session::put('params', $params);
        \Session::save();

        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername(config('billing.paypal_username'));
        $gateway->setPassword(config('billing.paypal_password'));
        $gateway->setSignature(config('billing.paypal_signature'));
        $gateway->setTestMode(true);


        $response = $gateway->purchase($params)->send();


        if ($response->isSuccessful()) {

            event(new PaymentMade($receipt));

            //setFlash('success_msg','Payment made successfully!');

            //return redirect()->route('my.service',$item->contract->getId());
        } elseif ($response->isRedirect()) {

            // redirect to offsite payment gateway
            $response->redirect();
        } else {
            // payment failed: display message to customer
            echo $response->getMessage();
        }
    }

    public function getSuccessPayment(Request $request)
    {


        $params = \Session::get('params');
        $v = Validator::make($params,[
            'amount'=>'required|numeric|min:1',
            'description'=>'required',
            'name'=>'required',
            'cancelUrl'=>'required|url',
            'returnUrl'=>'required|url',
            'completeUrl'=>'required|url',
            'payable_id'=>'required|int',
            'payable_type'=>'required|model',
            'receipt'=>'required|object'
        ]);


        if ($v->fails()){
            setFlash('error_msg','Invalid request');
            //return new NotFoundException();
            return $v->errors();
        }

        $item = array_get($params,'receipt');
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername('borderklan_api1.gmail.com');
        $gateway->setPassword('W56CT8S3KPGFHGL7');
        $gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31A774o5POD11SJ9ONZpEEpl1N6a30');
        $gateway->setTestMode(true);

        $response = $gateway->completePurchase($params)->send();
        $paypalResponse = $response->getData(); // this is the raw response object

       // dd($paypalResponse);

        if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {


            $item->paid = 1;
            $item->date_paid =  date('Y-m-d H:i:s');
            $item->save();

            event(new PaymentMade($item));

            setFlash('success_msg','Payment made successfully!');

            return redirect()->to($params['completeUrl']);

        } else {
            $item->paid = 1;
            $item->date_paid =  date('Y-m-d H:i:s');
            $item->save();

            event(new PaymentMade($item));

            setFlash('success_msg','Payment made successfully!');

            return redirect()->to($params['completeUrl']);
            /*
            dd($response->getMessage());
            setFlash('error_msg','Ooops! Failed, invalid request');
            return redirect()->to($params['completeUrl']);
            */
        }

    }

    public function destroy($item)
    {
        $params = \Session::get('params');

        dd($params);

        $v = Validator::make($params,[
            'amount'=>'required|numeric|min:1',
            'description'=>'required',
            'name'=>'required',
            'cancelUrl'=>'required|url',
            'returnUrl'=>'required|url',
            'payable_id'=>'required|int',
            'payable_type'=>'required|model'
        ]);

        setFlash('warning_msg','Payment receipt deleted successfully!');

        return redirect()->back();
    }
}