<?php
namespace App\Modules\Billing\Http\Controllers;

use App\Bill;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Caffeinated\Themes\Facades\Theme;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class BillsController extends Controller
{
    use FormBuilderTrait;
    protected $user;
    protected $item;

    public function __construct(Bill $item)
    {
        $this->user = user();
        $this->item = $item;
    }

    public function index()
    {
        if (hasRole(1)){
            $items = $this->item->all();
        }else{
            $items = $this->user->bills;
        }

        return Theme::view('billing::bills.index',compact('items'));
    }


    public function show($bill)
    {
        return Theme::view('billing::bills.show',compact('bill'));
    }

}
