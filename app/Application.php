<?php

namespace App;

use App\Events\BillCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Stevebauman\EloquentTable\TableTrait;

class Application extends Model
{
    use TableTrait;
  protected  $table = 'applications';

  protected $fillable = [
      'form_id','user_id','status_id','applicable_id','applicable_type','token',
      'published','submission_date','created_by','application_no'
  ];

    public function __toString()
    {
        return $this->application_no.' - '.$this->form;
    }

    public function applicable()
  {
      return $this->morphTo();
  }


  public function author()
  {
      return $this->belongsTo(User::class,'created_by');
  }

  public function customer()
  {
      return $this->belongsTo(User::class,'user_id');
  }

  public function form()
  {
      return $this->belongsTo(Form::class,'form_id');
  }

  public function status()
  {
      return $this->belongsTo(Status::class,'created_by');
  }

  public function data()
  {
      return $this->hasMany(FormData::class,'application_id');
  }

  public function saveForm($data)
  {
      foreach($data as $dt)
      {
          $exists = FormData::whereApplicationId($this->id)->whereFormFieldId($dt['form_field_id'])->first();
          if ($exists){
              $exists->value = $dt['value'];
              $exists->save();
          }else{
              FormData::create($dt);
          }
      }

      $this->data;
  }

  public function getData()
  {
      $data = [];
      foreach($this->data as $dt){
          $data['form_field_id'][$dt->form_field_id] = $dt->value;
      }

      return $data;
  }
  public function scopeProfileApplications($query,$user = null)
  {
      if (is_null($user)){
          $user = user()->id;
      }

      $ids = Property::whereCreatedBy($user)->lists('id')->toArray();

      return $query->whereIn('applicable_id',$ids)
          ->where('applicable_type','App\Property');
  }

  public function scopePublished($query,$bool = 1)
  {
      return $query->where('published','=',$bool);
  }

  public function scopeStatus($query,$status_id = 1)
  {
      return $query->where('status_id','=',$status_id);
  }

  public function scopeLatest($query)
  {
      return $query->orderBy('created_at','DESC');
  }



  public function scopeMyApplications($query,$user_id =  null)
  {
      if (is_null($user_id)){
          $user_id = user()->id;
      }

      return $query->where('user_id','=',$user_id);
  }

  public function deleteData()
  {
      return DB::table('form_datas')->where('application_id','=',$this->id)->delete();
  }

  public function updateStatus(array $details)
  {
      $user = user();

      if (array_key_exists('name',$details)){
          $this->$details['name'] = $details['value'];
          $this->viewed = 1;
          $this->save();

          $status = $this->status;


      }else{
          $this->update($details);
      }
     // dd($this->status_id);



      return $this;
  }

  public function generateBill()
      {
          $cost = $this->form->fee;
          $period = 1;
          $total = $cost * $period;
          $cycle = 1;

          $bill = Bill::create([
              'billable_id'=>$this->id,
              'billable_type'=>get_class($this),
              'user_id'=>$this->user_id,
              'items'=>$period,
              'cost'=>$cost,
              'total'=>$total,
              'description'=>"Bill for Service $this @ KES $cost for $cycle"
          ]);

          $bill->setBillNo();

          $this->billed = 1;

          event(new BillCreated($bill));

          return $bill;
      }

    public function submit()
    {
        $this->update(['published'=>1,'submission_date'=>date("Y-m-d H:i:s")]);
        //fire bill submitted here
    }


    public function uploadFile($file)
    {
        Storage::put(
            'applications/'.$this->id,
            file_get_contents($file->getRealPath())
        );
    }

    public function getFile($link)
    {

    }
}
