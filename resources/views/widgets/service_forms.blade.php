<button class="btn btn-primary" data-toggle="modal" data-target="#frmNewFormModal"><i class="fa fa-plus"></i> Add Form </button>
<table class="table table-framed">
    <thead>
    <tr>
        <th ># </th>
        <th class="col-xs-3">Form Name</th>
        <th class="col-xs-2">Date Created</th>
        <th>Description</th>
        <th>Created By</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
    <tr>
        <td>{{$item->id}}</td>
        <td><span class="text-semibold">{{$item}}</span></td>
        <td>{{$item->created_at}}</td>
        <td>{!! $item->description !!}</td>
        <td>{!! $item->author !!}</td>
        <td>
            <a href="{{route('forms.show',$item->id)}}" class="btn btn-primary btn-sm"> <i class="fa fa-folder"></i> </a>
            <a href="{{route('forms.edit',$item->id)}}" class="btn btn-info"> <i class="fa fa-pencil"></i> </a>
        </td>
    </tr>
    @endforeach

    </tbody>

</table>

<p>{!! $item->render() !!}</p>