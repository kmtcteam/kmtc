<?php

return [
    /**
     * define the admin url
     */
    'admin_url'=>'backend',
    /**
     * define the User model
     */
    'user_model'=>'\App\User',

    /**
     * role_model
     */
    'role_model'=>'\App\Role',

    /**
     * Permission model
     */
    'permission_model'=>'\App\Permission'
];