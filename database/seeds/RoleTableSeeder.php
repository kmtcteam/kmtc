<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //\DB::table('roles')->truncate();

        $roles = [
          [
          'name'=>'Administrator',
          'slug'=>'root',
          'special'=>'all-access',
          'category'=>'Backend Access'
        ],
        [
        'name'=>'Student',
        'slug'=>'student',
        'special'=>null,
        'category'=>'Frontend Access'
        ]
        ];

      foreach ($roles  as $role) {
        \App\Role::create($role);
      }
    }
}
