<div class="navbar navbar-default" id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav">
            <li class="active"><a href="{{route('my.account')}}"><i class="icon-display4 position-left"></i> Dashboard</a></li>


            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-make-group position-left"></i> Applicatioins <span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-250">
                    <li class="dropdown-header">My Applications</li>
                    <li><a href="{{route('my.applications')}}"><i class="icon-task"></i> Applications</a></li>
                    <li><a href="{{route('my.drafts')}}"><i class="icon-task"></i> Drafts</a></li>

                    <li class="dropdown-header">Apply for Service</li>
                    @foreach (\App\Category::all() as $element)
                        <li><a href="{{route('my.forms',$element->id)}}">{{$element->name}}s</a></li>

                    @endforeach

                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-make-group position-left"></i> Billing<span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-250">
                    <li><a href="{{route('my.bills')}}">My Bills</a></li>
                    <li><a href="{{route('forms.index')}}">My Payments</a></li>
                </ul>
            </li>


        </ul>


    </div>
</div>


