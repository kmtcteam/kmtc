<script src="{{ Theme::asset('js/libs/jquery-1.10.1.min.js')}}"></script>
<script src="{{ Theme::asset('js/libs/jquery-ui-1.9.2.custom.min.js')}}"></script>
<script src="{{ Theme::asset('js/libs/bootstrap.min.js')}}"></script>

<!--[if lt IE 9]>
<script src="{{ Theme::asset('js/libs/excanvas.compiled.js')}}"></script>
<![endif]-->

<!-- Plugin JS -->
<script src="{{ Theme::asset('js/plugins/icheck/jquery.icheck.js')}}"></script>
<script src="{{ Theme::asset('js/plugins/select2/select2.js')}}"></script>
<script src="{{ Theme::asset('js/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ Theme::asset('js/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
<script src="{{ Theme::asset('js/plugins/simplecolorpicker/jquery.simplecolorpicker.js')}}"></script>
<script src="{{ Theme::asset('js/plugins/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{ Theme::asset('js/plugins/textarea-counter/jquery.textarea-counter.js')}}"></script>
<script src="{{ Theme::asset('js/plugins/fileupload/bootstrap-fileupload.js')}}"></script>

<!-- App JS -->
<script src="{{ Theme::asset('js/target-admin.js')}}"></script>

